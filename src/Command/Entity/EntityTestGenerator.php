<?php

namespace PTT\Command\Entity;

class EntityTestGenerator
{
    public function run($argv = [])
    {
        print(PHP_EOL);
        print("\033[32m+-------------------------------------------------------------------+" . PHP_EOL);
        print("| Welcome to Entity Unit Test generator                             |" . PHP_EOL);
        print("|                                                                   |" . PHP_EOL);
        print("| Version: 1                                                        |" . PHP_EOL);
        print("+-------------------------------------------------------------------+\033[0m" . PHP_EOL);
        print(PHP_EOL);

        print("\033[33mUsage: read-entities :entitiesPath {:projectRootReplacements}\033[0m" . PHP_EOL . PHP_EOL);
        print("\033[34mParams\033[0m" . PHP_EOL . PHP_EOL);
        print(":entitiesPath = (required) Where your entities are placed" . PHP_EOL);
        print(":projectRootReplacements = Replacements for namespace. Comma separated Eg: app,lib,vendor,system..." . PHP_EOL . PHP_EOL);
        print("\033[34mAn example for replacements: \033[31msrc/AppBundle/Entity\033[34m will be treated as \\AppBundle\\Entity \033[0m" . PHP_EOL . PHP_EOL);
        print("\033[91mAttention: if you use shell auto-complete please remove the trailing dash '/' at the end of Entity folder\033[0m");

        if (isset($argv[1])) {
            $filter = ['src/', 'app/', 'lib/', 'vendor/', 'example/'];
            if (isset($argv[2])) {
                $receivedFilters = explode(',', $argv[2]);
                $receivedFilters = array_filter($receivedFilters, 'strlen');
                foreach ($receivedFilters as $receivedFilter) {
                    $filter[] = $receivedFilter . '/';
                }
            }

            $path = filter_var($argv[1]);
            $autoGeneratedPath = __DIR__ . '/../../../_ptt_auto-generated';

            if (!is_dir($autoGeneratedPath)) {
                mkdir($autoGeneratedPath);
            }

            $namespace = explode('/', str_replace($filter, '', $path));
            $namespace = array_values(array_filter($namespace, 'strlen'));
            $namespace = "\\" . implode("\\", $namespace) . "\\";

            $entities = glob(__DIR__ . '/../../../' . $path . '/*.php');

            $targetPath = $autoGeneratedPath . '/' . str_replace($filter, '', $path);

            if (!is_dir($targetPath)) {
                mkdir($targetPath, 0777, true);
            }

            foreach ($entities as $entity) {
                $entityName = explode('/', $entity);
                $entityClassName = str_replace('.php', '', end($entityName));
                $testClassName = str_replace('.php', 'Test', end($entityName));
                $entityName = str_replace('.php', 'Test.php', end($entityName));
                $testClass = $targetPath . '/' . $entityName;

                if (!file_exists($testClass)) {
                    $classMethods = get_class_methods($namespace . str_replace('Test.php', '', $entityName));

                    $getters = [];
                    $setters = [];
                    foreach ($classMethods as $classMethod) {
                        if (strpos($classMethod, 'get') === 0) {
                            $getters[] = "$classMethod()";
                        }
                        if (strpos($classMethod, 'is') === 0) {
                            $getters[] = "$classMethod()";
                        }
                        if (strpos($classMethod, 'set') === 0) {
                            $setters[] = "{$classMethod}('1');";
                        }
                    }

                    $settersForClass = '';
                    foreach ($setters as $setter) {
                        $settersForClass .= '        $entity->' . $setter . PHP_EOL;
                    }

                    $asserts = '';
                    foreach ($getters as $getter) {
                        $asserts .= '        $this->assertNotNull($entity->' . $getter . ');' . PHP_EOL;
                    }

                    $namespaceForClass = substr($namespace, 1, (strlen($namespace) - 2));

                    $testClassContent = <<<EOF
<?php 
      
namespace Tests\Unit\\$namespaceForClass;
        
use PHPUnit_Framework_TestCase;
use $namespaceForClass\\$entityClassName;
        
/**
 * Class $testClassName
 * @package Tests\Unit\\$namespaceForClass
 *
 * @group Unit
 * @group Entity
 */
class $testClassName extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkGettersAndSetters()
    {
        \$entity = new $entityClassName();
                
$settersForClass
$asserts    }
}

EOF;
                    file_put_contents($testClass, $testClassContent);

                    echo PHP_EOL . PHP_EOL . "\033[94mWrote Entity Test Case in " . PHP_EOL . $testClass . "\033[0m" . PHP_EOL . PHP_EOL;
                }
            }
        }
    }
}
